class Vehicle {
  constructor(maxSpeed, length) {
    this.maxSpeed = maxSpeed;
    this.length = length;
  }

  start() {
    console.log("start vehicle with maxspeed: " + this.maxSpeed);
  }
}

class Boat extends Vehicle {
  constructor(maxSpeed, length, licenseNeeded) {
    super(maxSpeed, length);
    this.licenseNeeded = licenseNeeded;
  }

  start() {
    console.log("start boat. License needed: " + this.licenseNeeded);
  }
}

class Bike extends Vehicle {
  constructor(maxSpeed, length, windShieldSurface) {
    super(maxSpeed, length);
    this.windShieldSurface = windShieldSurface;
  }
}

let b = new Bike(10, 3, 4);
b.start();
let boat = new Boat(20, 5, true);
boat.start();
boat.start("hi");
