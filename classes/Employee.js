class Employee {
  // the constructor function is called when we create
  // Employee objects using the new keyword
  constructor(id, firstName, lastName, jobTitle, payRate) {
    this.employeeId = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.jobTitle = jobTitle;
    this.payRate = payRate;
  }

  sayHi(nr, firstName) {
    for (let i = 0; i < nr; i++) {
      console.log("HI! I'm " + this.firstName + " confusing: " + firstName);
    }
  }

  getFullName() {
    return this.firstName + " " + this.lastName;
  }

  getIntro() {
    let intro =
      "Hi! I'm " + this.getFullName() + " and I am a " + this.jobTitle;
    return intro;
  }
}

let employee1 = new Employee(1, "Ian", "Auston", "Graphic Artist", 42.5);
// let intro = employee1.getIntro();
console.log(employee1.getIntro());
