let menu = [
  ["eggs", "coffee", "oats"],
  ["eggs", "coffee"],
  ["potatoes", "tea", "yoghurt"],
];

// find coffee
function findCoffee(items) {
  for (let i = 0; i < items.length; i++) {
    if (items[i].toLowerCase() == "coffee") {
      return true;
    }
  }
  return false;
}

function findCoffeeStr(item) {
  if (item.toLowerCase() == "coffee") {
    return true;
  } else {
    return false;
  }
}

function findCoffeeInArr(items) {
  let result = items.find(findCoffeeStr);
  if (result != undefined) {
    return true;
  } else {
    return false;
  }
}

let result1 = menu.find(findCoffee);
let result1a = menu.find(findCoffeeInArr);
let result1b = menu.find(
  (items) => items.find((item) => item.toLowerCase() == "coffee") != undefined
);
console.log(result1a);

// findIndex of coffee
let result2 = menu.findIndex(findCoffee);
console.log(result2);

// filter for coffee
let result3 = menu.filter(findCoffee);
console.log(result3);


let kids = [
  { first: "Natalie", last: "Plyers", specialQuality: "" },
  { first: "Brittany", last: "Ray", specialQuality: "" },
  { first: "Zachary", last: "Westly", specialQuality: "" },
];
function buildFullName(arrayElement) {
  return arrayElement.first + " " + arrayElement.last;
}

function addProperty(arrayElement) {
  arrayElement.first = arrayElement.first.toUpperCase();
  arrayElement.new = "weird thing to do though";
  return arrayElement;
}

function allOnes(arrayElement) {
  return 1;
}

let namesList = kids.map(allOnes);
console.log(namesList);
let numElements = namesList.length;
for (let i = 0; i < numElements; i++) {
  console.log(namesList[i]);
  // output matches image above
}




