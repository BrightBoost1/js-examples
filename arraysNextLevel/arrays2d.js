let teamMembers = [
  ["Dana", "Brenda", "Happy"],
  ["Laura", "Patti"],
  ["Leslye", "Randy", "Mollye", "Ranse"],
  ["Eloise", "Robert"],
];

teamMembers.forEach((team) => {
  console.log("-----------");
  console.log("Team " + teamMembers.indexOf(team));
  console.log("-----------");
  teamMembers[teamMembers.indexOf(team)].forEach((m) => console.log(m));
});
