"use strict";

let name = "Cody";
console.log("Hi", name);
console.log(typeof name);
console.log(name);

let word = "blabkla" * 1;
let num = 3 / 0;
console.log(word);
console.log(num);
let variableName;
console.log(variableName);

let dec = 3.4;
// console.log(result);
let cold = true;

let nr = 4.523823;
console.log(nr.toFixed(3));

let randomNr = Math.floor(Math.random() * 100)

let hoi = "hoi";
let hoi2 = parseFloat(hoi); //NaN
console.log(typeof hoi2); //NaN is a number!

let guessWhat = null;
console.log(typeof guessWhat);

const x = 2;
x = 4;

let tryThis = 1;

switch(tryThis) {
  case 1:
    console.log("test1");
    break;
  case 2:
    console.log("test2");
    break;
  case 3:
    console.log("test3");
    break;
}

function test2A() {
  if (1 == 1) {
    var a = 5;
  }
  console.log("a = " + a);
}
test2A();
  console.log("a = " + a);


let d = new Date();
d.setMonth(1);
d.setDate(9);
d.setFullYear(1963);

console.log(d);

