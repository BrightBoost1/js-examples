"use strict";

function processInput() {
  // get the input fields
  const name = document.getElementById("nameInput");
  const age = document.getElementById("ageInput");
  const dogYears = document.getElementById("dogYears");

  // get the values
  let userName = name.value;
  let userAge = age.value;

  // set value for dogYears
  dogYears.value = userAge * 7;

  // create the message
  let message = `Hi ${userName}, you're ${userAge} years old.`;

  // set the message as innerHTML of the paragraph
  let paragraph = document.getElementById("hiParagraph");
  paragraph.innerHTML = message;
}