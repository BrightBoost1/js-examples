let i = 5;
let name = "maaike";

if(i == 6) {
  console.log("i is 6!");
}

if(i < 10) {
  console.log("i is smaller than 10");
} else if(i == 10) {
  console.log("i equals 10");
} else {
  console.log("i is bigger than 10");
}

switch(name) {
  case "maaike":
    console.log("oh I know her");
    break;
  case "toni":
    console.log("oh, say hi to Toni!");
    break;
  default:
    console.log("I don't know this person");
    break;

}

function someName() {
  console.log("doing stuff in the function");
}

someName();

Math.ceil(7.1);
