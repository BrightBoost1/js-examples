"use strict";

window.onload = function () {
  const helloBtn = document.getElementById("helloBtn");
  helloBtn.onclick = onHelloBtnClicked;
  helloBtn.onmouseover = onHelloBtnMouseOver;
  helloBtn.onmouseout = onHelloBtnMouseOut;
};

function onHelloBtnClicked() {
  alert("Hi there!");
}

function onHelloBtnMouseOver() {
  const para = document.getElementById("para");
  para.innerHTML = "Hi!!";
}

function onHelloBtnMouseOut() {
  const para = document.getElementById("para");
  para.innerHTML = "";
}
