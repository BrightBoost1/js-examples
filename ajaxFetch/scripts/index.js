window.onload = function () {
  const btn = document.getElementById("callAPI");
  const postBtn = document.getElementById("postBtn");
  const submitCourse = document.getElementById("myCourse");
  submitCourse.onsubmit = onSubmitCourse;
  btn.onclick = onClickButton;
  postBtn.onclick = onPostButtonClick;
};

function onClickButton() {
  let element = document.getElementById("messageArea");
  let id = document.getElementById("userId").value;
  let url = "http://jsonplaceholder.typicode.com/users/" + id;
  fetch(url)
    .then((response) => {
      if (response.status >= 200 && response.status < 300) {
        return response.json();
      } else {
        throw new Error();
      }
    })
    .then((data) => {
      let message = "User email: " + data.email;
      element.innerHTML = message;
    })
    .catch((err) => console.log(err));
}

function onPostButtonClick() {
  let newCourse = {
    dept: "Finance",
    courseNum: "401",
    courseName: "Tax Laws",
    instructor: "Maaike",
    startDate: "Aug 1",
    numDays: 5,
  };
  let url = "http://localhost:8081/api/courses/";
  fetch(url, {
    method: "POST",
    body: JSON.stringify(newCourse),
    headers: {
      "Content-type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  })
    .then((response) => {
      if (response.status >= 200 && response.status < 300) {
        return response.json();
      } else {
        throw new Error();
      }
    })
    .then((data) => {
      console.dir(data);
    })
    .catch((err) => console.log(err));
}

function onSubmitCourse() {
  let data = new FormData(document.getElementById("myCourse"));
  // data.append("dept", document.getElementById("dept").value);
  // data.append("courseName", document.getElementById("courseName").value);
  // data.append("instructor", document.getElementById("instructor").value);
  // data.append("startDate", document.getElementById("startDate").value);
  // data.append("courseNum", document.getElementById("courseNum").value);
  // data.append("numDays", document.getElementById("numDays").value);

  console.log(...data);
  // Send the request. Do not call JSON.stringify. Do not
  // set HTTP header
  fetch("http://localhost:8081/api/coursesform/", {
    method: "POST",
    body: data
  })
    .then((response) => response.json())
    .then((json) => {
      console.dir(json);
    })
    .catch((err) => {
      console.dir(err);
      return false;
    });


  return false;
}
