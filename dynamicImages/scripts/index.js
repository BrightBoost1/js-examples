let imageFiles = [
  { name: "images/kitten.jpeg", description: "A cute cat" },
  { name: "images/pup.jpeg", description: "A cute dog" },
];

window.onload = function () {
  populateImagesDropDown();
  // register event handlers for buttons
  document.getElementById("add").onclick = onClickAddButton;
  document.getElementById("clear").onclick = onClickClearButton;
};

function populateImagesDropDown() {
  const selectBox = document.getElementById("animalImages");

  for (let i = 0; i < imageFiles.length; i++) {
    let option = new Option(imageFiles[i].description, imageFiles[i].name);
    selectBox.appendChild(option);
  }
}

function onClickAddButton() {
  const imagesDiv = document.getElementById("displayImages");
  const selectedImageIndex =
    document.getElementById("animalImages").selectedIndex;
  // create img element
  let img = document.createElement("img");
  img.src = imageFiles[selectedImageIndex].name;
  img.alt = imageFiles[selectedImageIndex].description;
  img.width = 300;
  imagesDiv.appendChild(img);
}

function onClickClearButton() {
  const imagesDiv = document.getElementById("displayImages");
  imagesDiv.innerHTML = "";
}
