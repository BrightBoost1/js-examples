function greet() {
  console.log("Hi!");
}

function square(x) {
  return x * x;
}

function isPositive(x) {
  return x >= 0;
}

function subtract(x, y) {
  return x - y;
}

function biggestOfTwo(x, y) {
  if (x > y) {
    return x;
  } else if (x < y) {
    return y;
  }
}

function findBiggest(arr) {
  let biggest = arr[0];
  for (let i = 1; i < arr.length; i++) {
    if (biggest < arr[i]) {
      biggest = arr[i];
    }
  }
  return biggest;
}

let nrs = [4, 5, 223, 45, 5656, 22332];
console.log(findBiggest(nrs));

let greet = () => console.log("Hi!");

let square = (x) => x * x;

let isPositive = (x) => x >= 0;

let subtract = (x, y) => x - y;

// condition ? return for true : return for false

let biggestOfTwo = (x, y) => (x > y ? x : x < y ? y : undefined);

let findBiggest = (arr) => {
  let biggest = arr[0];
  for (let i = 1; i < arr.length; i++) {
    if (biggest < arr[i]) {
      biggest = arr[i];
    }
  }
  return biggest;
};

// elegant solution by Dan
let fa = (a) => Math.max(...a);
console.log(fa([4, 1, 12, 8]));
