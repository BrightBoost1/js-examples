function add(a, b) {
  return a + b;
}

function print(word) {
  console.log(word);
}

// add equivalent
let f1 = (a, b) => a + b;
console.log(f1(2, 4));

// print
let f2 = (s1, s2) => console.log(s1, s2);
f2("JavaScript", "fun");
console.log(f2);

function find(arr, f) {
  for(let i = 0; i < arr.length; i++) {
    if(f(arr[i])) {
      return arr[i];
    }
  }
}

let kids = [
  { first: "Natalie", last: "Plyers" },
  { first: "Brittany", last: "Ray" },
  { first: "Zachary", last: "Westly" },
];

kids.forEach(e => e.first + " " + e.last);

let firstLetters = kids.map(e => e.first[0]);

