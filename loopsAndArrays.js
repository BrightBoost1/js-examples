let courses = [
  {
    CourseId: "PROG100",
    Title: "Introduction to HTML/CSS/Git",
    Location: "Classroom 7",
    StartDate: "09/08/22",
    Fee: "100.00",
  },
  {
    CourseId: "PROG200",
    Title: "Introduction to JavaScript",
    Location: "Classroom 9",
    StartDate: "11/22/22",
    Fee: "350.00",
  },
  {
    CourseId: "PROG300",
    Title: "Introduction to Java",
    Location: "Classroom 1",
    StartDate: "01/09/23",
    Fee: "50.00",
  },
  {
    CourseId: "PROG400",
    Title: "Introduction to SQL and Databases",
    Location: "Classroom 7",
    StartDate: "03/16/23",
    Fee: "50.00",
  },
  {
    CourseId: "PROJ500",
    Title: "Introduction to Angular",
    Location: "Classroom 1",
    StartDate: "04/25/23",
    Fee: "50.00",
  },
];

let under50Dollars = maxPriceRangeCourses(courses, 50);

for (let i = 0; i < under50Dollars.length; i++) {
  console.log(under50Dollars[i].Title);
}

// What are the titles of the courses that cost $50 or less?

function maxPriceRangeCourses(courses, max) {
  let matching = [];
  for (let i = 0; i < courses.length; i++) {
    if (Number(courses[i].Fee) <= max) {
      matching.push(courses[i]);
    }
  }

  return matching;
}

let academyMembers = [
  {
    memID: 101,
    name: "Bob Brown",
    films: ["Bob I", "Bob II", "Bob III", "Bob IV"],
  },
  {
    memID: 142,
    name: "Sallie Smith",
    films: ["A Good Day", "A Better Day"],
  },
  {
    memID: 187,
    name: "Fred Flanders",
    films: ["Who is Fred?", "Where is Fred?", "What is Fred?", "Why Fred?"],
  },
  {
    memID: 203,
    name: "Bobbie Boots",
    films: ["Walking Boots", "Hiking Boots", "Cowboy Boots"],
  },
];

let startsWithBob = nameBeginsWith(academyMembers, "Bob");

for (let i = 0; i < startsWithBob.length; i++) {
  console.log(startsWithBob[i]);
}

function nameBeginsWith(academymembers, start) {
  let matching = [];
  for (let i = 0; i < academyMembers.length; i++) {
    if (academyMembers[i].name.startsWith(start)) {
      matching.push(academyMembers[i]);
    }
  }
  return matching;
}

function customAlphabeticalSort(arr, field) {
  let sorter = function (a, b) {
    return a[field].compare(b[field]);
  };

  arr.sort(sorter);
  // arr.sort((a, b) => a[field].compare(b[field])); // writing it with an arrow function
}

customSort(courses, "Title");
console.log(courses)

