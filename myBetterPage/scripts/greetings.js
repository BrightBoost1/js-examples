function sayHi() {
  const messageDiv = document.getElementById("messageDiv");
  messageDiv.innerHTML = "Hi all!";
}

function changeColor() {
  const messageDiv = document.getElementById("messageDiv");
  messageDiv.style.color = "green";
}
