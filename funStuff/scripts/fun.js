function transformPuppy() {
  // get the image by id
  const puppyImg = document.getElementById("puppy");
  // change the src attribute on click
  if (puppyImg.src.includes("big")) {
    puppyImg.src = "../../images/puppy.jpg";
    puppyImg.alt = "tiny puppy";
  } else {
    puppyImg.src = "../../images/bigpuppy.jpg";
    puppyImg.alt = "big puppy and bonus puppy";
  }
}
