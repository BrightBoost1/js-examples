let emp = { name: "Bobby", job: "Make people happy" };

console.log("dot: ", emp.name);
console.log("dot: ", emp.job);
console.log("[]: ", emp["name"]);

let variable = "job";
console.log("[var]: ", emp[variable]);

let person = { name: "Pursalane", age: 11, gender: "Female" };
for (let key in person) {
  console.log(key + " = " + person[key]);
}

for (let i = 0; i < 50; i++) {
  if (i % 2 == 0) {
    continue;
  }
  console.log(i);
}

for (let i = 0; i < 50; i++) {
  if (i % 2 != 0) {
    console.log(i + '\nnew "line');
  }
}

for (let i = 0; i < 7; i++) {
  console.log("I love loops!");
}

let i = 0;

while (i < 7) {
  console.log("I love loops");
  i++;
}

let menu = [
  { item: "Hamburger", price: 6.95 },
  { item: "Cheeseburger", price: 7.95 },
  { item: "Hot dog", price: 4.95 },
];

console.log(menu[1]["price"]);
let emp1 = {
  name: "Gaia",
  tasks: ["stuff to do", "diy stuff etc", "chat a lot"],
};

let employees = [
  { name: "Mark", tasks: ["stuff to do", "coding etc", "meetings"] },
  emp1,
  { name: "Eric" },
];

employees[1].tasks[2];

let menu2 = [
  ["eggs", "coffee", "oats"],
  ["eggs", "coffee"],
  ["potatoes", "tea"],
];

console.log(menu2[0][1]);

let myOrder = [
  { item: "Chicken Tacos", price: 8.95 },
  { item: "Guacamole", price: 2.85 },
  { item: "Sweet Tea", price: 2.75 },
];
let yourOrder = [
  { item: "Hamburger", price: 6.95 },
  { item: "Fries", price: 2.25 },
  { item: "Sweet Tea", price: 2.75 },
  { item: "Fried Apple Pie", price: 4.95 },
];
let mealCost = getMealCost(myOrder);
let totalWithTip = mealCost * 1.2;
console.log("My meal costs " + totalWithTip.toFixed(2));

function getMealCost(orders) {
  let sum = 0;
  let numOrders = orders.length;
  for (let i = 0; i < numOrders; i++) {
    sum += orders[i].price;
  }
  return sum;
}

let silly = ["something", "silly", "very", "creativet"];

// for(let item of silly) {
//   console.log(item);
// }

for (let i = 0; i < silly.length; i++) {
  if (silly[i].indexOf("et") != -1) {
    console.log(
      silly[i] +
        " contains et at position: " +
        i +
        " string position: " +
        silly[i].indexOf("et")
    );
  }
}

let word = "wiEsje";
console.log(word.search(/es/g));