window.onload = function () {
  let imageGroups = document.getElementsByClassName("imageGroup");
  for (let i = 0; i < imageGroups.length; i++) {
    imageGroups[i].style.border = "2px solid red";
  }

  let noAltText = document.querySelectorAll('img[alt=""], img:not([alt])');
  for (let i = 0; i < noAltText.length; i++) {
    noAltText[i].alt = "puppy";
  }

};

function removePuppy() {
  let imageToBeDeleted = document.getElementById("saleImg");
  imageToBeDeleted.parentNode.removeChild(imageToBeDeleted);
}
