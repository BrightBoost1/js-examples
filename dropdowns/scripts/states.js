window.onload = function () {
  initiateDropdownStates();
  document.getElementById("statesList").onchange = onShowInfoStateClick;
  document.getElementById("showInfoState").onclick = onShowInfoStateClick;
};

function initiateDropdownStates() {
  const statesList = document.getElementById("statesList");
  let states = ["Alabama", "Alaska", "Arizona"];
  let abbrev = ["AL", "AK", "AZ"];

  for (let i = 0; i < states.length; i++) {
    let option = new Option(states[i], abbrev[i]);
    option.id = i;
    statesList.appendChild(option);
    console.dir(option);
  }
}

function onShowInfoStateClick() {
  const p = document.getElementById("infoState");
  const statesList = document.getElementById("statesList");

  if (statesList.value != null) {
    p.innerHTML =
      statesList.options[statesList.selectedIndex].id +
      " " +
      statesList.options[statesList.selectedIndex].text;
  }
}
